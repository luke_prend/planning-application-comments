<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentsForm extends Mailable
{
    use Queueable, SerializesModels;

    private $reference;
    private $comments;
        
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reference, $comments)
    {
        $this->reference = $reference;
        $this->comments  = $comments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[PAPP:'.$this->reference.'] Application Comments';
        
        return $this->view('emails.commentsForm')
            ->from('no_reply@gisnorthlincs.co.uk', 'GIS Northlincs')
            ->subject($subject)
            ->with('reference', $this->reference)
            ->with('comments', $this->comments);
    }
}
