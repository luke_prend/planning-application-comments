<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;
use App\Comment;
use App\Mail\CommentsForm;

class CommentsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCommentForm($id)
    {
        $this->setApplicationDetails($id);
        
        return view('comments')->with('applicationDetails', $this->applicationDetails);
    }
    
    /**
     * Get application details from Salesforce
     * 
     * @param type $id
     */
    private function setApplicationDetails($id)
    {
        Forrest::Authenticate();
        $applicationReference = $this->correctReferenceFormat($id);
        
        $sqlQuery = '
            SELECT 
                Id,
                Name,
                NL_Print_Address__c,
                Proposal__c,
                Validation_Date__c, 
                Easting_and_Northing__c,
                Decision_Date__c,
                Parishes__c,
                Wards__c,
                PlanningOfficer__c
            FROM PApplication__c 
            WHERE Name = \''.$applicationReference.'\'
            LIMIT 1';
        
        $query = Forrest::query($sqlQuery);
        // get the records returned by the Forrest query
        $this->applicationDetails = $query['records'][0];
    }
    
    /**
     * Change reference to correct format
     * 
     * @param type $ref
     * @return type string
     */
    private function correctReferenceFormat($ref)
    {
        return strtoupper(str_replace("-", "/", $ref));
    }
    
    /**
     * Change reference to correct format for url
     * 
     * @param type $ref
     * @return type string
     */
    private function urlReferenceFormat($ref)
    {
        return strtolower(str_replace("/", "-", $ref));
    }
    
    /**
     * Store a new comment.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeComment(Request $request, $id)
    {
        $this->validate($request, [
            'reference' => 'required',
            'comments'  => 'required|max:10000',
        ]);
        
        Comment::Create([
            'planning_app_reference' => $request->get('reference'),
            'comment' => $request->get('comments'),
        ]);
        
        // send email to planningapplications@northlincs.gov.uk
        $this->sendEmail($request->get('reference'), $request->get('comments'));
        
        // set details to display reference on comments page
        $this->setApplicationDetails($this->urlReferenceFormat($request->get('reference')));
        
        return view('comments')
                ->with('applicationDetails', $this->applicationDetails)
                ->with('success', 'Thank you. You have successfully submitted a comment relating to planning application <strong>'.$request->get('reference').'</strong>.')
                ->with('id', $id);
    }
    
    /**
     * Send confirmation email
     * 
     * @param type $reference
     * @param type $comments
     */
    private function sendEmail($reference, $comments)
    {
        \Mail::to('planningapplications@northlincs.gov.uk')
                ->send(new CommentsForm($reference, $comments));
    }
}
