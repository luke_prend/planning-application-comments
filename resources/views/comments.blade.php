@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $applicationDetails['Name'] }}</div>
                                
                
                <div class="panel-body">
                    @if (isset($success))
                    
                    <div class="alert alert-success">
                        <strong>Success!</strong> <br />
                        {!! $success !!}
                    </div>
                    
                    <a href="{{ route('comments', $id) }}" class="btn btn-primary">
                        Submit another Comment
                    </a>
                    
                    @else 
                    
                    <form class="form-horizontal" method="POST" action="">
                        <input type="hidden" name="reference" value="{{ $applicationDetails['Name'] }}" />
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Proposal</label>
                            </div>
                            <div class="col-md-9">
                                {{ $applicationDetails['Proposal__c'] }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Ward</label>
                            </div>
                            <div class="col-md-9">
                                {{ $applicationDetails['Wards__c'] }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Parish</label>
                            </div>
                            <div class="col-md-9">
                                {{ $applicationDetails['Parishes__c'] }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Case Officer</label>
                            </div>
                            <div class="col-md-9">
                                {{ $applicationDetails['PlanningOfficer__c'] }}
                            </div>
                        </div>
                        
                        <label>Comments</label>

                        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <textarea id="comments" class="form-control" rows="10" name="comments">
                                </textarea>
                                
                            @if ($errors->has('comments'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('comments') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 center">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                    @endif 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
