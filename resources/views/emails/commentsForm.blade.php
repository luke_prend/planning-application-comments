<!DOCTYPE html>

<html>
    
<head>
    
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:800,700,600,400,300">
    
</head>

<body style="font-family: 'Open Sans', sans-serif">

    <h1>Planning Application Comment</h1>
    <p>A new comment has been submitted. Details are below:</p>
    
    <p>
        <strong>Reference: </strong>{{ $reference }}<br />
        <strong>Comments: </strong>{{ $comments }}<br />
    </p>
    
</body>

</html>
